def leer_archivo_por_linea(ruta):
    with open(ruta, 'r') as archivo:
        for linea in archivo:
            yield linea.strip()

for linea in leer_archivo_por_linea('biblia.txt'):
    print(linea)
    print('ha terminado la linea\n')