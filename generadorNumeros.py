def generador_numeros(n):
    for i in range(n):
        yield i


gen = generador_numeros(5)
for num in gen:
    print(num)