def generador_pares(limite):
    num = 0
    while num <= limite:
        yield num
        num += 2

for par in generador_pares(10):
    print(par)