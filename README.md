# GENERADORES EN PYTHON
## ¿Que son los generadores?
Los generadores en Python son una forma especial de funciones que permiten crear iteradores. A diferencia de las funciones regulares que devuelven una sola vez un valor con **return**, los generadores utilizan **yield** para producir una serie de valores a lo largo del tiempo, uno a la vez, pausing su ejecución y manteniendo su estado entre cada producción.

Los generadores son muy eficientes ya que no almacenan todos los valores en memoria. En su lugar, generan cada valor en la marcha, lo cual es útil cuando se manejan grandes secuencias de datos. De la misma manera los generadores pueden mantener su estado entre llamadas, lo que permitir pausar y reanudar su ejecución. lo cual no es posible con una función regular.

## Sintaxis básica
```
def generador_numeros(n):
    for i in range(n):
        yield i

# usamos el generador
gen = generador_numeros(5)
for num in gen:
    print(num)

```
Para mirar el anterior ejemplo da  [click aquí](https://gitlab.com/python3433226/generadores/python-generadores/-/blob/master/generadorNumeros.py?ref_type=heads)
