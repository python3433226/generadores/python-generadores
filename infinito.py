def generador_infinito():
    num = 0
    while True:
        yield num
        num += 1

gen = generador_infinito()

for _ in range(10):
    print(next(gen))